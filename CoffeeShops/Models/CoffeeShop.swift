//
//  CoffeeShop.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/21/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
class CoffeeShop: Object {
    dynamic var id = ""
    dynamic var desc = ""
    dynamic var name = ""
    dynamic var favorite = false
    dynamic var url = ""
    dynamic var location : Location?
    dynamic var phoneNumber = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(json: JSON) {
        self.init()
        name            = json["name"].stringValue
        id              = json["id"].stringValue
        desc            = json["description"].stringValue
        url             = json["url"].stringValue
        let contact     = json["contact"]
        phoneNumber     = contact["phone"].stringValue
        location        = Location(json: json["location"])

    }
    
    class func arrayFrom(jsonArray: [JSON]) -> [CoffeeShop]{
        var coffeeShops = [CoffeeShop]()
        for json in jsonArray {
            let venueJson = json["venue"]
            if  let coffeeShop = CoffeeShop(json: venueJson) {
                do {
                    let realm = try Realm()
                    if let currentObject = try! realm.object(ofType: CoffeeShop.self, forPrimaryKey: coffeeShop.id as AnyObject) {
                        coffeeShop.favorite = currentObject.favorite
                    }
                } catch {
                    print(">>> error trying to save coffe shops")
                }

                coffeeShops.append(coffeeShop)
            }
        }
        do {
            let realm = try Realm()
            try realm.write({ () -> Void in
                realm.add(coffeeShops, update: true)
            })
        } catch {
            print(">>> error trying to save coffe shops")
        }

        return coffeeShops
    }
    
    class func favorite(coffeeShop: CoffeeShop) -> Bool {
        do {
            let realm = try Realm()
            try realm.write({ () -> Void in
                coffeeShop.favorite = !coffeeShop.favorite
            })
        } catch {
            print(">>> error trying to save coffe shops")
        }
        return coffeeShop.favorite
    }
    
    class func favourites() -> [CoffeeShop] {
        var favourites = [CoffeeShop]()
        do {
            let realm = try Realm()
            let results = realm.objects(CoffeeShop.self).filter({ (coffeeShop) -> Bool in
                coffeeShop.favorite == true
            })
            favourites.append(contentsOf: results)
        } catch {
            print(">>> error trying to save coffe shops")
        }
        return favourites
    }
}
