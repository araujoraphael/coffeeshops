//
//  Location.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/21/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
class Location: Object {
    dynamic var address = ""
    dynamic var city = ""
    dynamic var state = ""
    dynamic var postalCode = ""
    dynamic var lat = 0.0
    dynamic var lng = 0.0
    dynamic var distance = 0.0
    
    required convenience init?(json: JSON) {
        self.init()
        address    = json["address"].stringValue
        city       = json["city"].stringValue
        state      = json["state"].stringValue
        postalCode = json["postalCode"].stringValue
        lat        = json["lat"].doubleValue
        lng        = json["lng"].doubleValue
        distance   = json["distance"].doubleValue
    }
}
