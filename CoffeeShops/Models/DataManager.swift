//
//  DataManager.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/21/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import SystemConfiguration
class DataManager: AnyObject {
    class func getNearbyCoffeeShops(onCompletion:@escaping (_ error: Bool, _ message: String, _ result: [CoffeeShop]) -> Void) {
        let client_id = "UOJ24NDYP4VJG2SGVMEKRNE0D4EFT30T0EF0IEOSI4DB3DJ2"
        let client_secret = "SLZOPYABHLNEXAQMYNPQNASDHSKGOOBQHAUIN3MYE2WZSHXJ"
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let userLocation = appDelegate.locationManager.location {
            let url = "https://api.foursquare.com/v2/venues/explore?ll=\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)&client_id=\(client_id)&client_secret=\(client_secret)&v=20170221&categoryId=4bf58dd8d48988d1e0931735&openNow=1"
            
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                if let responseJSON = response.result.value {
                    let json = JSON(responseJSON)
                    let response = json["response"]
                    let groups = response["groups"].arrayValue.first
                    let items = groups!["items"].arrayValue
                    
                    let coffeeShops = CoffeeShop.arrayFrom(jsonArray: items)
                    
                    if(coffeeShops.count > 0){
                        
                        onCompletion(false, "Success!", coffeeShops)
                    } else {
                        onCompletion(true, "No nearby open!", coffeeShops)
                    }
                    
                } else {
                    onCompletion(true, "Request error!", [CoffeeShop]())
                }

            })
        }
    }
    
    class func getFavourites() -> [CoffeeShop] {
        return CoffeeShop.favourites()
    }
    
    class func favortieCoffeeShop(coffeeShop: CoffeeShop) -> Bool {
        return CoffeeShop.favorite(coffeeShop: coffeeShop)
    }
}
