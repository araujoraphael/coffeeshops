//
//  ViewController.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/21/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit
import MapKit
import ReachabilitySwift
class CoffeeShopsViewController: UIViewController{
    @IBOutlet weak var mapView : MKMapView!
    @IBOutlet weak var tableView : UITableView!
    var coffeeShops = [CoffeeShop]()
    let screenBounds = UIScreen.main.bounds
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.titleView = UIImageView(image: UIImage(named:"logo_navbar"))
        self.tableView.register(UINib(nibName: "CoffeeShopTableViewCell", bundle: nil), forCellReuseIdentifier: "CoffeeShopTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(CoffeeShopsViewController.reachabilityChanged(_:)),name: ReachabilityChangedNotification,object: appDelegate.reachability)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.centerMap()
        if let reachability = appDelegate.reachability {
            if(reachability.isReachable) {
                self.loadData()
            } else {
                let alertController = UIAlertController(title: "Oops!", message: "Not connected to internet", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        } else {
            //something is wrong. Try, anyway!
            self.loadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
        DataManager.getNearbyCoffeeShops { (error, message, coffeeShops) in
            if(!error) {
                if(coffeeShops.count > 0) {
                    self.coffeeShops = coffeeShops
                    self.addCoffeeShopsAnnotations()
                    self.tableView.reloadData()
                }else {
                    print("erro 0")
                }
                
            } else {
                print("erro")
                
                //               let alertController = UIAlertController(title: "", message: <#T##String?#>, preferredStyle: UIAlertControllerStyle)
            }
        }
    }

    func addCoffeeShopsAnnotations() {
        self.mapView.removeAnnotations(self.mapView.annotations)
        for coffeeShop in self.coffeeShops {
            if let coffeeShopLocation = coffeeShop.location {
                let location = CLLocation(latitude: coffeeShopLocation.lat, longitude: coffeeShopLocation.lng)
                let annotation = CoffeeShopAnnotation(coordinate: location.coordinate, image: UIImage(named: "map_marker")!, coffeeShop: coffeeShop)
                self.mapView.addAnnotation(annotation)
            }
        }
    }
    
    func reachabilityChanged(_ status: NSNotification) {
        
        let reachability = status.object as! Reachability
        
        if reachability.isReachable{
            self.loadData()
        } else {
            print("Network not reachable")
        }
    }

}

extension CoffeeShopsViewController: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coffeeShops.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CoffeeShopTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CoffeeShopTableViewCell") as! CoffeeShopTableViewCell
        cell.coffeeShop = self.coffeeShops[indexPath.row]
        return cell
    }
    
    //MARK: UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "coffeeShopDetailsSegue", sender: self.coffeeShops[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "coffeeShopDetailsSegue") {
            let coffeeShop = sender as! CoffeeShop
            let destinationVc = segue.destination as! CoffeeShopDetailsViewController
            destinationVc.coffeeShop = coffeeShop
        }
    }
}

extension CoffeeShopsViewController: MKMapViewDelegate {
    func centerMap() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let userLocation = appDelegate.locationManager.location {
            let region = MKCoordinateRegionMakeWithDistance(
                userLocation.coordinate, 1000, 1000)
            
            self.mapView.setRegion(region, animated: true)
            
            let centerCoordinate = self.mapView.convert(CGPoint(x: screenBounds.width/2, y: self.mapView.frame.height/2), toCoordinateFrom: self.mapView)
            self.mapView.setCenter(centerCoordinate, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
        if annotationView == nil{
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        let coffeeAnnotation = annotation as! CoffeeShopAnnotation
        var favoritedStr = ""
        if (coffeeAnnotation.coffeeShop.favorite){
            favoritedStr = "_favorited"
        }
        annotationView?.image = UIImage(named: "map_marker\(favoritedStr)")
        annotationView?.centerOffset = CGPoint(x: 0, y: -((annotationView?.frame.size.width)!/2))
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let coffeeShopAnnotation = view.annotation as? CoffeeShopAnnotation {
            let coffeeShop = coffeeShopAnnotation.coffeeShop
            self.performSegue(withIdentifier: "coffeeShopDetailsSegue", sender: coffeeShop)
        }
    }
}

class CoffeeShopAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var image: UIImage!
    var highLightedImage: UIImage!
    var coffeeShop: CoffeeShop!
    init(coordinate: CLLocationCoordinate2D, image: UIImage, coffeeShop: CoffeeShop) {
        self.coordinate = coordinate
        self.image = image
//        self.highLightedImage = highLightedImage
        self.coffeeShop = coffeeShop
    }
}


