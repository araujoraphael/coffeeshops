//
//  CoffeeShopDetailsViewController.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/21/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit
class CoffeeShopDetailsViewController: UIViewController {
    @IBOutlet weak var coffeeOverLayerView : UIView!
    @IBOutlet weak var coffeeCupImageView : UIImageView!
    @IBOutlet weak var coffeeShopImageView : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var overImageView : UIView!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var phoneButton : UIButton!
    @IBOutlet weak var favoriteButton : UIButton!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var fullScreenImageContainer : UIView!
    
    var coffeeShop : CoffeeShop?
    let maskView = UIView(frame: UIScreen.main.bounds)
    let fullscreenImageView = UIImageView(frame: UIScreen.main.bounds)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []

        self.tableView.register(UINib(nibName: "CoffeeShopDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "CoffeeShopDetailTableViewCell")

        self.nameLabel.text = self.coffeeShop?.name
                
        if let favorite = self.coffeeShop?.favorite {
            self.coffeeCupImageView.isHighlighted = favorite
        }
        
        if let favorite = self.coffeeShop?.favorite {
            self.favoriteButton.isSelected = favorite
        }
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 300.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CoffeeShopDetailsViewController.tapImageOverLayer(_:)))
        self.coffeeOverLayerView.addGestureRecognizer(tapGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showFullScreenImage(_: UITapGestureRecognizer){
        self.fullScreenImageContainer.isHidden = false
        UIView.animate(withDuration: 0.1) { 
            self.fullScreenImageContainer.alpha = 1.0
        }
    }
    
    func hideFullScreenImage(_: UITapGestureRecognizer){
        UIView.animate(withDuration: 0.1, animations: {
            self.fullScreenImageContainer.alpha = 0

        }) { (succeess) in
            self.fullScreenImageContainer.isHidden = true
        }
    }
    
    @IBAction func phoneButtonTapped(sender: AnyObject) {
        if let phone = self.coffeeShop?.phoneNumber {
            if(phone != "") {                
                if let url = URL(string: "tel://\(phone)") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            } else {
                print("The establishment don't have a phone number")
            }
        }
    }

    @IBAction func backButtonTapped(sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func webSiteButtonTapped(sender: AnyObject) {
        if let urlString = self.coffeeShop?.url {
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func favoriteButtonTapped(sender: AnyObject) {
        if let coffeeShop = self.coffeeShop {
            let favorited = DataManager.favortieCoffeeShop(coffeeShop: coffeeShop)
            self.coffeeCupImageView.isHighlighted = favorited
            self.favoriteButton.isSelected = favorited
        }
    }
    
    func tapImageOverLayer(_ gestureRecognizer: UITapGestureRecognizer) {
        self.fullscreenImageView.image = self.coffeeShopImageView.image
        self.fullscreenImageView.contentMode = .scaleAspectFit
        self.fullscreenImageView.alpha = 0.0
        
        self.maskView.backgroundColor = UIColor.init(white: 0, alpha: 0)
        self.maskView.addSubview(self.fullscreenImageView)
        self.view.window?.addSubview(self.maskView)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.maskView.backgroundColor = UIColor.init(white: 0, alpha: 0.8)
            self.fullscreenImageView.alpha = 1.0
        }) { (succeess) in
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CoffeeShopDetailsViewController.tapMaskView(_:)))
            self.maskView.addGestureRecognizer(tapGesture)
        }
    }
    
    func tapMaskView(_ gestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.2, animations: {
            self.maskView.backgroundColor = UIColor.init(white: 0, alpha: 0.0)
            self.fullscreenImageView.alpha = 0.0

        }) { (succeess) in
            self.maskView.removeGestureRecognizer(gestureRecognizer)
            self.maskView.removeFromSuperview()
            self.fullscreenImageView.removeFromSuperview()
        }
    }
}
    extension CoffeeShopDetailsViewController: UITableViewDelegate, UITableViewDataSource {
        
        //MARK: UITableViewDataSource methods
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : CoffeeShopDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CoffeeShopDetailTableViewCell") as! CoffeeShopDetailTableViewCell
            cell.coffeeShop = self.coffeeShop
            return cell
        }
    }
