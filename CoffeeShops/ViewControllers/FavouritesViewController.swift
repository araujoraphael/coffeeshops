//
//  FavouritsViewController.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/21/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit
import RealmSwift
class FavouritesViewController: UIViewController {
    @IBOutlet weak var tableView : UITableView!
    var favourites = [CoffeeShop]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "FavoriteTableViewCell", bundle: nil), forCellReuseIdentifier: "FavoriteTableViewCell")
        self.loadData()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
        self.favourites = DataManager.getFavourites()
        self.tableView.reloadData()
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension FavouritesViewController: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: UITableViewDataSource methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "coffeeShopDetailsSegue") {
            let coffeeShop = sender as! CoffeeShop
            let destinationVc = segue.destination as! CoffeeShopDetailsViewController
            destinationVc.coffeeShop = coffeeShop
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favourites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FavoriteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTableViewCell") as! FavoriteTableViewCell
        let favorite = self.favourites[indexPath.row]
        cell.coffeeShop = favorite
        return cell
        
    }

}

