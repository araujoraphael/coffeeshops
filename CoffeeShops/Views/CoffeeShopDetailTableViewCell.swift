//
//  CoffeeShopDetailTableViewCell.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/23/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit
import MapKit
class CoffeeShopDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var coffeeCupImageView : UIImageView!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    @IBOutlet weak var mapView : MKMapView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var coffeeShop: CoffeeShop? {
        didSet {
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let cs = coffeeShop else {
            return
        }
        
        if cs.desc != "" {
            self.descLabel.text = cs.desc
        } else {
            self.descLabel.text = "No description available"
        }
        
        self.addressLabel.text = ""

        if let address = cs.location?.address, address != "" {
            self.addressLabel.text = "\(address),\n"
        }
        
        if let city = cs.location?.city, city != "" {
            self.addressLabel.text = self.addressLabel.text?.appending("\(city)")
        }
        
        if let state = cs.location?.state, state != "" {
            self.addressLabel.text = self.addressLabel.text?.appending(", \(state)")
        }
        
        if let postalCode = cs.location?.postalCode, postalCode != "" {
            self.addressLabel.text = self.addressLabel.text?.appending(", \(postalCode)")
        }
        self.centerMap()
    }
}

extension CoffeeShopDetailTableViewCell: MKMapViewDelegate {
    func centerMap() {
        if let coffeeShopLocation = self.coffeeShop?.location, coffeeShopLocation.lat != 0.0, coffeeShopLocation.lng != 0.0 {
            let coordinate = CLLocationCoordinate2D(latitude: coffeeShopLocation.lat, longitude: coffeeShopLocation.lng)
            let region = MKCoordinateRegionMakeWithDistance(coordinate, 1000, 1000)
            
            self.mapView.setRegion(region, animated: true)
            
            let centerCoordinate = self.mapView.convert(CGPoint(x: self.frame.size.width/2, y: self.mapView.frame.height/2), toCoordinateFrom: self.mapView)
            self.mapView.setCenter(centerCoordinate, animated: true)
            
            let location = CLLocation(latitude: coffeeShopLocation.lat, longitude: coffeeShopLocation.lng)
            let annotation = CoffeeShopAnnotation(coordinate: location.coordinate, image: UIImage(named: "map_marker")!, coffeeShop: self.coffeeShop!)
            self.mapView.addAnnotation(annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
        if annotationView == nil{
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "map_marker")
        annotationView?.centerOffset = CGPoint(x: 0, y: -((annotationView?.frame.size.width)!/2))
        return annotationView
    }
}
