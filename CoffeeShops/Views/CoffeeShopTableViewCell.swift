//
//  CoffeShopTableViewCell.swift
//  CoffeeShops
//
//  Created by Raphael Araújo on 2/21/17.
//  Copyright © 2017 araujoraphael. All rights reserved.
//

import UIKit

class CoffeeShopTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var distanceLabel : UILabel!
    @IBOutlet weak var coffeeShopImage : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var coffeeShop: CoffeeShop? {
        didSet {
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let cs = coffeeShop else {
            return
        }
        
        self.nameLabel.text = cs.name
        
        if var distance = cs.location?.distance {
            distance = distance/1000 //M to KM
            self.distanceLabel.text = distance < 1 ? String(format: "%.1fKM away", distance) : String(format: "%.0fKM away", distance)
        }
        
        self.coffeeShopImage.isHighlighted = cs.favorite
    }
    
}
